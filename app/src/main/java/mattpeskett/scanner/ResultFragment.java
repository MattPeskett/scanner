package mattpeskett.scanner;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ResultFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ResultFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ResultFragment extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private TextView value;
    private Button action;
    private ImageView image;
    private String result;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ResultFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ResultFragment newInstance(String param1, String param2) {
        ResultFragment fragment = new ResultFragment();
        fragment.setStyle(DialogFragment.STYLE_NO_FRAME, 0);
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ResultFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations = R.style.MyAnimation_Window;
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v =  inflater.inflate(R.layout.fragment_result, container, false);
        value = (TextView)v.findViewById(R.id.ScannedItem);
        action = (Button)v.findViewById(R.id.ButtonToClick);
        image = (ImageView)v.findViewById(R.id.ResultImage);
        result = getArguments().getString("result");
        if(result.startsWith("BEGIN"))
        {
            String name = result.substring(14, 26);
            value.setText("New Contact: \n" + name);
            action.setText("Add Contact");
            if(name.equals("Peter Wilson"))
                image.setImageResource(R.drawable.pete_img);
            else if(name.equals("Matt Peskett"))
                image.setImageResource(R.drawable.matt_img);
            else {
                image.setImageResource(R.drawable.not_found_img);
                Toast.makeText(getActivity(), "Image not found", Toast.LENGTH_SHORT).show();
            }

            action.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v)
                {
                    OutputStreamWriter osw = null;
                    File outputFile;
                    try {
                        File outputDir = getActivity().getCacheDir();
                        outputFile = File.createTempFile("prefix", "extension", outputDir);
                        FileOutputStream fOut = new FileOutputStream(outputFile);
                        osw = new OutputStreamWriter(fOut);
                        // write vcard string to file
                        osw.write(result);
                        // ensures that all buffered bytes are written
                        osw.flush();
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setDataAndType(Uri.fromFile(outputFile), "text/vcard");
                        startActivity(i);
                    } catch (Resources.NotFoundException e) {
                        // ..
                    } catch (IOException e) {
                        // ...
                    } finally {
                        if (osw != null) {
                            try {
                                osw.close();
                            } catch (IOException e) {
                                // ...
                            }
                        }
                    }
                    // let the os handle the import of the vcard to the Contacts
                    // application
                    /*
                         Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, "");
                        startActivity(intent);
                        */
                }
            });
        }
        else {
            if(result.equals("062300709823")) {
                value.setText("Glade - Clean Linen");
                image.setImageResource(R.drawable.glade_img);
            }
            else if(result.equals("056500363800")) {
                value.setText("One a Day Gummies");
                image.setImageResource(R.drawable.gummies_img);
            }
            else {
                value.setText("Unrecognized");
                image.setImageResource(R.drawable.not_found_img);
            }
            action.setText("Buy Product");
            action.setOnClickListener(new View.OnClickListener() {
                  public void onClick(View v) {
                      String address = null;
                      if(result.equals("062300709823"))
                          address = "http://www.amazon.ca/SC-Johnson-13200-Glade-Clean/dp/B001O0QV9Y/ref=sr_1_4?ie=UTF8&qid=1449036338&sr=8-4&keywords=glade+clean+linen";
                      else if(result.equals("056500363800"))
                          address = "http://www.amazon.ca/Fruiti-ssentials-Multivitamin-Gummies-Adults-Tablets/dp/B00JNWDEKO/ref=sr_1_2?ie=UTF8&qid=1449036937&sr=8-2&keywords=one+a+day+gummies";

                      if(address == null){
                          Toast.makeText(getActivity(), "Cannot find Product", Toast.LENGTH_SHORT).show();
                          return;
                      }
                      Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(address));
                      startActivity(browserIntent);
                  }
              });
        }

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}

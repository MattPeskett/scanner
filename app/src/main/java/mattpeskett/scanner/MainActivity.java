package mattpeskett.scanner;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends Activity implements ResultFragment.OnFragmentInteractionListener {

    int SCAN_RESULT = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void ScanCode(View v)
    {
        Intent intent = new Intent(this, ScanBarcode.class);
        startActivityForResult(intent, SCAN_RESULT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Get the result from the code
        if (requestCode == SCAN_RESULT) {
            if (resultCode == RESULT_OK) {
                FragmentManager fm = getFragmentManager();
                ResultFragment editNameDialog = new ResultFragment();
                Bundle args = new Bundle();
                args.putString("result", data.getStringExtra("RESULT"));
                editNameDialog.setArguments(args);
                editNameDialog.show(fm, "fragment_result");
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
